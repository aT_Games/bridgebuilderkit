﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace BBK
{
    [System.Serializable]
    public class LevelData
    {
        public static int VERSIONNUMBER = 4;
        
        //Settings used for persistence
        public static string LEVELDATAPATH = "Data/LevelData/";
        public static string SAVEDATAPATH = "Data/SaveData/";
        public static string RESOURCEPATH = "BridgeBuilderKit/Resources/";
        
        //Data used by launcher
        public string name;
        public GameObject levelIndicator;
        public bool isCompleted;
        public bool isSkipped;
        public bool isUnlocked;

        //Starting data used by level
        public int versionNumber = 0;
        public string backgroundSprite;
        public float scaleFactor;
        public int boardCount;
        public int levelTimer;
        public Vector2 runnerStartPos;
        public Vector2 runnerEndPos;
        public List<AnchorSaveData> startingAnchorPoints = new List<AnchorSaveData>();
        public List<CoverSprite> coverObjects = new List<CoverSprite>();
        public List<Vector2> colliderPoints = new List<Vector2>();
        public List<string> storyImages = new List<string>();
        
        //Data added by player at run-time
        public List<GirderSaveData> girders = new List<GirderSaveData>();
        public List<AnchorSaveData> anchors = new List<AnchorSaveData>();
        
        private int girderLineCount = 5;
        private int anchorLineCount = 3;



        /// <summary>
        /// Default constructor, used by LevelManager
        /// </summary>
        public LevelData()
        {

        }


        /// <summary>
        /// Constructor used by GameManager to construct the class with the default level data
        /// </summary>
        /// <param name="defaultText"></param>
        public LevelData(TextAsset defaultText, string saveDataText = null)
        {
            string[] lines = defaultText.text.Split('\n');
            int lineNumber;
            
            name = defaultText.name;
            
            //Fix issue, remove legacy
            if(lines[0].Contains("VersionNumber"))
            {
                int version = int.Parse(lines[1]);
                if(version == 1)
                {
                    ParseLevelDataVersion1(lines);
                }
                if(version == 2)
                {
                    ParseLevelDataVersion2(lines);
                }
                if(version == 3)
                {
                    ParseLevelDataVersion3(lines);
                }
                if (version == 4)
                {
                    ParseLevelDataVersion4(lines);
                }

                StoryManager.instance.SetStory(storyImages);
            }
            else
            {
                ParseLevelDataVersion0(lines);
            }
            
            if(saveDataText != null && File.Exists(saveDataText))
            {
                saveDataText = File.ReadAllText(saveDataText);

               lines = saveDataText.Split('\n'); 
               
               //Get girders
               int recordCount = int.Parse(lines[1]);
               
               lineNumber = 2;
               for(int i = 0; i < recordCount; i++)
               {
                   GirderSaveData gsd = new GirderSaveData();
                    gsd.girderName = string.Format("Girder{0}", girders.Count);
                   gsd.point1.x = float.Parse(lines[lineNumber + 1]);
                   gsd.point1.y = float.Parse(lines[lineNumber + 2]);
                   gsd.point2.x = float.Parse(lines[lineNumber + 3]);
                   gsd.point2.y = float.Parse(lines[lineNumber + 4]);
                   girders.Add(gsd);
                   lineNumber += girderLineCount;
               }
               
               //Get anchors
               recordCount = int.Parse(lines[lineNumber + 1]);
               
               lineNumber += 2;
               for(int i = 0; i < recordCount; i++)
               {
                   AnchorSaveData asd = new AnchorSaveData();
                   asd.anchorName = lines[lineNumber].Trim();
                   asd.position.x = float.Parse(lines[lineNumber + 1]);
                   asd.position.y = float.Parse(lines[lineNumber + 2]);
                   anchors.Add(asd);
                   lineNumber += anchorLineCount;
               }
            }
        }
        
        ///Save default level data 
        public void SaveDefaultsToFile()
        {
            string filePath = string.Format("{0}/{1}{2}", Application.dataPath, RESOURCEPATH, LEVELDATAPATH);
            
            if(!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            filePath = string.Format("{0}{1}.txt", filePath, name);
            
            using (StreamWriter writer = new StreamWriter(File.Open(filePath,FileMode.Create)))
            {
                writer.WriteLine("//VersionNumber");
                writer.WriteLine(VERSIONNUMBER);
                writer.WriteLine("//Background sprite");
                writer.WriteLine(backgroundSprite);
                writer.WriteLine("//Scalefactor");
                writer.WriteLine(scaleFactor);
                writer.WriteLine("//Number of boards");
                writer.WriteLine(boardCount);
                writer.WriteLine("//Time to run");
                writer.WriteLine(levelTimer);
                writer.WriteLine("//Runner Start X");
                writer.WriteLine(runnerStartPos.x);
                writer.WriteLine("//Runner Start Y");
                writer.WriteLine(runnerStartPos.y);
                writer.WriteLine("//Runner End X");
                writer.WriteLine(runnerEndPos.x);
                writer.WriteLine("//Count of anchors");
                writer.WriteLine(startingAnchorPoints.Count);
                writer.WriteLine("//Anchor positions");
                
                foreach(AnchorSaveData anchor in startingAnchorPoints)
                {
                    writer.WriteLine(anchor.anchorName);
                    writer.WriteLine((int)anchor.type);
                    writer.WriteLine(anchor.position.x);
                    writer.WriteLine(anchor.position.y);
                }
                
                writer.WriteLine("//Count of cover");
                writer.WriteLine(coverObjects.Count);
                writer.WriteLine("//Cover positions");
                foreach(CoverSprite cover in coverObjects)
                {
                    writer.WriteLine(cover.position.x);
                    writer.WriteLine(cover.position.y);
                    writer.WriteLine(cover.spriteName);
                }
                
                writer.WriteLine("//Count of edge collider points");
                writer.WriteLine(colliderPoints.Count);
                foreach(Vector2 point in colliderPoints)
                {
                    writer.WriteLine(point.x);
                    writer.WriteLine(point.y);
                }

                //Make sure we have the latest story
                storyImages = StoryManager.instance.GetStory();

                writer.WriteLine("//Count of story slides");
                writer.WriteLine(storyImages.Count);
                foreach (string story in storyImages)
                {
                    writer.WriteLine(story);
                }
            }

        }
        
        public void SaveLevelDataToFile()
        {
            string filePath = string.Format("{0}/{1}", Application.persistentDataPath, SAVEDATAPATH);
            
            if(!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            filePath = string.Format("{0}{1}.txt", filePath, name);

            Debug.Log(string.Format("Saving too: {0}", filePath));

            using (StreamWriter writer = new StreamWriter(File.Open(filePath,FileMode.Create)))
            {
                writer.WriteLine("Count of girders:");
                writer.WriteLine(girders.Count);
                foreach(GirderSaveData girder in girders)
                {
                    writer.WriteLine(girder.girderName);
                    writer.WriteLine(girder.point1.x);
                    writer.WriteLine(girder.point1.y);
                    writer.WriteLine(girder.point2.x);
                    writer.WriteLine(girder.point2.y);
                }
                writer.WriteLine("Count of anchors:");
                writer.WriteLine(anchors.Count);
                foreach(AnchorSaveData anchor in anchors)
                {
                    writer.WriteLine(anchor.anchorName);
                    writer.WriteLine(anchor.position.x);
                    writer.WriteLine(anchor.position.y);
                }
            }
            
            #if UNITY_EDITOR
            UnityEditor.AssetDatabase.Refresh();
            #endif
        }
            
        void ParseLevelDataVersion0(string[] lines)
        {
            int anchorCount;
            int coverCount;
            int lineNumber;

            //Parse data
            scaleFactor = float.Parse(lines[1]);
            boardCount = int.Parse(lines[3]);
            levelTimer = int.Parse(lines[5]);

            runnerStartPos = new Vector2(float.Parse(lines[7]), float.Parse(lines[9]));
            runnerEndPos = new Vector2(float.Parse(lines[11]), float.Parse(lines[9]));

            anchorCount = int.Parse(lines[13]);
            lineNumber = 15;
            //Parse starting anchors
            for(int i = 0; i < anchorCount; i++)
            {
                AnchorSaveData asd = new AnchorSaveData();
                asd.position.x = float.Parse(lines[lineNumber + (i * 2)]);
                asd.position.y = float.Parse(lines[lineNumber + 1 + (i * 2)]);
                startingAnchorPoints.Add(asd);
            }

            lineNumber += anchorCount * 2 + 1;
            coverCount = int.Parse(lines[lineNumber]);
            lineNumber += 2;
            //Parse cover objects
            for (int i = 0; i < coverCount; i++)
            {
                coverObjects.Add(new CoverSprite(float.Parse(lines[lineNumber + (i * 2)]), float.Parse(lines[lineNumber + 1 + (i * 2)]), null));
            }
        }
        
        void ParseLevelDataVersion1(string[] lines)
        {
            int anchorCount;
            int coverCount;
            int lineNumber;
            
            //Parse data
            versionNumber = int.Parse(lines[1]);
            backgroundSprite = lines[3].Trim();
            scaleFactor = float.Parse(lines[5]);
            boardCount = int.Parse(lines[7]);
            levelTimer = int.Parse(lines[9]);

            runnerStartPos = new Vector2(float.Parse(lines[11]), float.Parse(lines[13]));
            runnerEndPos = new Vector2(float.Parse(lines[15]), float.Parse(lines[13]));

            anchorCount = int.Parse(lines[17]);
            lineNumber = 19;
            //Parse starting anchors
            for(int i = 0; i < anchorCount; i++)
            {
                AnchorSaveData asd = new AnchorSaveData();
                asd.position.x = float.Parse(lines[lineNumber + (i * 2)]);
                asd.position.y = float.Parse(lines[lineNumber + 1 + (i * 2)]);
                startingAnchorPoints.Add(asd);
            }

            lineNumber += anchorCount * 2 + 1;
            coverCount = int.Parse(lines[lineNumber]);
            lineNumber += 2;
            
            for (int i = 0; i < coverCount; i++)
            {
                coverObjects.Add(new CoverSprite(float.Parse(lines[lineNumber + (i * 3)]), float.Parse(lines[lineNumber + 1 + (i * 3)]), lines[lineNumber + 2 + (i*3)].Trim()));
            }
        }
        
        void ParseLevelDataVersion2(string[] lines)
        {
            int anchorCount;
            int coverCount;
            int colliderCount;
            int lineNumber;
            
            //Parse data
            versionNumber = int.Parse(lines[1]);
            backgroundSprite = lines[3].Trim();
            scaleFactor = float.Parse(lines[5]);
            boardCount = int.Parse(lines[7]);
            levelTimer = int.Parse(lines[9]);

            runnerStartPos = new Vector2(float.Parse(lines[11]), float.Parse(lines[13]));
            runnerEndPos = new Vector2(float.Parse(lines[15]), float.Parse(lines[13]));

            anchorCount = int.Parse(lines[17]);
            lineNumber = 19;
            //Parse starting anchors
            for(int i = 0; i < anchorCount; i++)
            {
                AnchorSaveData asd = new AnchorSaveData();
                asd.position.x = float.Parse(lines[lineNumber + (i * 2)]);
                asd.position.y = float.Parse(lines[lineNumber + 1 + (i * 2)]);
                startingAnchorPoints.Add(asd);
            }

            lineNumber += anchorCount * 2 + 1;
            coverCount = int.Parse(lines[lineNumber]);
            lineNumber += 2;
            
            for (int i = 0; i < coverCount; i++)
            {
                coverObjects.Add(new CoverSprite(float.Parse(lines[lineNumber + (i * 3)]), float.Parse(lines[lineNumber + 1 + (i * 3)]), lines[lineNumber + 2 + (i*3)].Trim()));
            }
            
            lineNumber += coverCount * 3 + 1;
            colliderCount = int.Parse(lines[lineNumber]);
            lineNumber += 1;
            
            for (int i = 0; i < colliderCount; i++)
            {
                colliderPoints.Add(new Vector2(float.Parse(lines[lineNumber + (i * 2)]), float.Parse(lines[lineNumber + 1 + (i * 2)])));
            }
        }
        
        void ParseLevelDataVersion3(string[] lines)
        {
            int anchorCount;
            int coverCount;
            int colliderCount;
            int lineNumber;
            
            //Parse data
            versionNumber = int.Parse(lines[1]);
            backgroundSprite = lines[3].Trim();
            scaleFactor = float.Parse(lines[5]);
            boardCount = int.Parse(lines[7]);
            levelTimer = int.Parse(lines[9]);

            runnerStartPos = new Vector2(float.Parse(lines[11]), float.Parse(lines[13]));
            runnerEndPos = new Vector2(float.Parse(lines[15]), float.Parse(lines[13]));

            anchorCount = int.Parse(lines[17]);
            lineNumber = 19;
            //Parse starting anchors
            for(int i = 0; i < anchorCount; i++)
            {
                AnchorSaveData asd = new AnchorSaveData();
                asd.anchorName = lines[lineNumber + (i * 4)].Trim();
                asd.type = (AnchorType)int.Parse(lines[lineNumber + 1 + (i * 4)]);
                asd.position.x = float.Parse(lines[lineNumber + 2 + (i * 4)]);
                asd.position.y = float.Parse(lines[lineNumber + 3 + (i * 4)]);
                startingAnchorPoints.Add(asd);
            }

            lineNumber += anchorCount * 4 + 1;
            coverCount = int.Parse(lines[lineNumber]);
            lineNumber += 2;
            
            for (int i = 0; i < coverCount; i++)
            {
                coverObjects.Add(new CoverSprite(float.Parse(lines[lineNumber + (i * 3)]), float.Parse(lines[lineNumber + 1 + (i * 3)]), lines[lineNumber + 2 + (i*3)].Trim()));
            }
            
            lineNumber += coverCount * 3 + 1;
            colliderCount = int.Parse(lines[lineNumber]);
            lineNumber += 1;
            
            for (int i = 0; i < colliderCount; i++)
            {
                colliderPoints.Add(new Vector2(float.Parse(lines[lineNumber + (i * 2)]), float.Parse(lines[lineNumber + 1 + (i * 2)])));
            }
        }

        void ParseLevelDataVersion4(string[] lines)
        {
            int anchorCount;
            int coverCount;
            int colliderCount;
            int storyCount;
            int lineNumber;

            //Parse data
            versionNumber = int.Parse(lines[1]);
            backgroundSprite = lines[3].Trim();
            scaleFactor = float.Parse(lines[5]);
            boardCount = int.Parse(lines[7]);
            levelTimer = int.Parse(lines[9]);

            runnerStartPos = new Vector2(float.Parse(lines[11]), float.Parse(lines[13]));
            runnerEndPos = new Vector2(float.Parse(lines[15]), float.Parse(lines[13]));

            anchorCount = int.Parse(lines[17]);
            lineNumber = 19;
            //Parse starting anchors
            for (int i = 0; i < anchorCount; i++)
            {
                AnchorSaveData asd = new AnchorSaveData();
                asd.anchorName = lines[lineNumber + (i * 4)].Trim();
                asd.type = (AnchorType)int.Parse(lines[lineNumber + 1 + (i * 4)]);
                asd.position.x = float.Parse(lines[lineNumber + 2 + (i * 4)]);
                asd.position.y = float.Parse(lines[lineNumber + 3 + (i * 4)]);
                startingAnchorPoints.Add(asd);
            }

            lineNumber += anchorCount * 4 + 1;
            coverCount = int.Parse(lines[lineNumber]);
            lineNumber += 2;

            for (int i = 0; i < coverCount; i++)
            {
                coverObjects.Add(new CoverSprite(float.Parse(lines[lineNumber + (i * 3)]), float.Parse(lines[lineNumber + 1 + (i * 3)]), lines[lineNumber + 2 + (i * 3)].Trim()));
            }

            lineNumber += coverCount * 3 + 1;
            colliderCount = int.Parse(lines[lineNumber]);
            lineNumber += 1;

            for (int i = 0; i < colliderCount; i++)
            {
                colliderPoints.Add(new Vector2(float.Parse(lines[lineNumber + (i * 2)]), float.Parse(lines[lineNumber + 1 + (i * 2)])));
            }

            lineNumber += colliderCount * 2 + 1;
            storyCount = int.Parse(lines[lineNumber]);
            lineNumber += 1;

            for (int i = 0; i < storyCount; i++)
            {
                storyImages.Add(lines[lineNumber + i].Trim());
            }

            
        }
    }
    
    //Data class for maintaining the name of the cover sprite along with its position
    public class CoverSprite
    {
        public Vector2 position;
        public string spriteName;
        
        public CoverSprite(float x, float y, string name)
        {
            position = new Vector2(x, y);
            spriteName = name;
        }
    }

}
