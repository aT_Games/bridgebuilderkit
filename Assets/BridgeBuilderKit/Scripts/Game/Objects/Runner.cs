﻿using UnityEngine;
using System.Collections;

namespace BBK
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Runner : MonoBehaviour
    {
        public Animator animator;
        
        public float speed = .5f;
        
        private bool isRunning = false;
        private bool isGrounded = false;
        private Rigidbody2D body;
        private GameManager gameManager;
        [HideInInspector]
        public float unitWidth;
        [HideInInspector]
        public float stoppingPoint;
        private bool reportedFinish = false;
        public delegate void FinishHandler(bool survivor);
        public FinishHandler Finished;
        
        // Use this for initialization
        void Start()
        {
            if (animator == null)
            {
                animator = gameObject.GetComponent<Animator>();
            }
            body = gameObject.GetComponent<Rigidbody2D>();
            body.isKinematic = true;
            gameManager = FindObjectOfType<GameManager>();
            
            unitWidth = gameObject.GetComponent<Collider2D>().bounds.size.x;
        }

        // Update is called once per frame
        void Update()
        {
            if(isRunning)
            {
                if(isGrounded && body.velocity.x < speed)
                {
                    body.AddForce(new Vector2(speed * body.mass * Time.deltaTime, 0), ForceMode2D.Impulse);
                }
                
                //Check if the runner fell 
                if(!reportedFinish && transform.position.y < gameManager.fallLine)
                {
                    if(Finished != null)
                    {
                        Finished(false);
                    }
                    reportedFinish = true;
                }
                
                //Check if the runner passed the finish line
                if(!reportedFinish && transform.position.x - (unitWidth/2f) >= gameManager.runnerEnd.x && transform.position.y >= gameManager.runnerEnd.y)
                {
                    if(Finished != null)
                    {
                        Finished(true);
                    }
                    reportedFinish = true;
                }
                
                if(transform.position.x >= stoppingPoint)
                {
                    StopRun();
                }
            }
        }



        public void StartRun()
        {
            body.isKinematic = false;
            isRunning = true;
            StartAnimation();
        }
        
        public void StopRun()
        {
            isRunning = false;
            StopAnimation();
            body.isKinematic = true;
        }
        
        void StartAnimation()
        {
            animator.SetFloat("RunSpeed", .1f);
        }

        void StopAnimation()
        {
            animator.SetFloat("RunSpeed", 0);
        }

        void OnCollisionStay2D(Collision2D col)
        {
            isGrounded = true;
        }
        
        void OnCollisionExit2D(Collision2D col)
        {
            isGrounded = false;
        }
    }
}
