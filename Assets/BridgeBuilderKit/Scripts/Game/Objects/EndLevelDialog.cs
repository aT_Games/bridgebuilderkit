﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace BBK
{
   
    public class EndLevelDialog : MonoBehaviour {

        public UIButton btnMainMenu;
        public UIButton btnReplay;
        public UIButton btnSkip;
        public Text txtTitle;
        public Text txtBody;
        public string successTitle;
        public string failureTitle;
        public string oneSuccessBody;
        public string twoSuccessBody;
        public string threeSuccessBody;
        public string failureBody;
        private CanvasGroup cg;
        private GameManager gameManager;
        // Use this for initialization
        void Start () {
            cg = GetComponent<CanvasGroup>();
            gameManager = FindObjectOfType<GameManager>();
            
            //Add button delegates
            btnMainMenu.Click += MapClicked;
            btnReplay.Click += RestartClicked;
            btnSkip.Click += SkipClicked;
        }
        
        public void SetScore(int score)
        {
            if(score > 0)
            {
                txtTitle.text = successTitle;
                
                if(score == 1)
                {
                     txtBody.text = string.Format("1 out of 3 made it across. \n {0}", oneSuccessBody);
                }
                if(score == 2)
                {
                     txtBody.text = string.Format("2 out of 3 made it across. \n {0}", twoSuccessBody);
                }
                if(score == 3)
                {
                     txtBody.text = string.Format("3 out of 3 made it across. \n {0}", threeSuccessBody);
                }
            }
            else
            {
                txtTitle.text = failureTitle;
                txtBody.text = failureBody;
            }
        }
        
        ///Show or hide the dialog
        public void ShowDialog(bool show)
        {
            if(show)
            {
                cg.alpha = 1;
                cg.interactable = true;
                cg.blocksRaycasts = true; 
            }
            else
            {
                cg.alpha = 0;
                cg.interactable = false;
                cg.blocksRaycasts = false;
            }
        }
        
        void MapClicked()
        {
            gameManager.MainMenu();
        }
        
        void RestartClicked()
        {
            gameManager.Restart();
        }

        void SkipClicked()
        {
            gameManager.Skip();
        }
        
        void OnDestroy()
        {
            //Remove button delegates
            btnMainMenu.Click -= MapClicked;
            btnReplay.Click -= RestartClicked;
            btnSkip.Click -= SkipClicked;
        }
    }
}
