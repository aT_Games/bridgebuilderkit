﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System;

namespace BBK
{


    public class Anchor : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IPointerEnterHandler
    {

        [TooltipAttribute("Regular anchors are only placed at run-time.")]
        public AnchorType type; //Is this anchor one that can move or not?
        [HideInInspector]
        public List<GameObject> connections = new List<GameObject>();
        [HideInInspector]
        public StructureType structureType;
        [HideInInspector]
        public List<Joint2D> joints = new List<Joint2D>();
        private DrawingManager drawingManager;

        void Start()
        {
            drawingManager = FindObjectOfType<DrawingManager>();
            
            //Use kinemetic option to keep anchors stationary until the simulation is running
            GetComponent<Rigidbody2D>().isKinematic = true;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (drawingManager.mode == DrawingMode.Building)
            {
                drawingManager.StartDraw(eventData.pointerCurrentRaycast.gameObject);
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (drawingManager.mode == DrawingMode.Building)
            {
                drawingManager.EndDraw();
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (drawingManager.mode == DrawingMode.Building)
            {
                drawingManager.Drag(Vector2.zero);
            }
        }
        
                
        public void AddConnection(GameObject connectTo, Vector2 point)
        {
            //Make sure no collisions are done between connected girders
            foreach(GameObject go in connections)
            {
                Physics2D.IgnoreCollision(connectTo.GetComponent<Collider2D>(), go.GetComponent<Collider2D>(), true);
            }
            
            connections.Add(connectTo);
            AddSpringJoint(connectTo, point);
        }
        
        public void RemoveConnection(GameObject girder)
        {
            Rigidbody2D connectedGirder = girder.GetComponent<Rigidbody2D>();
            joints.RemoveAll(x => x.connectedBody == connectedGirder);
            connections.Remove(girder);
        }
        
        private void AddDistanceJoint(GameObject connectTo, Vector2 point)
        {
            DistanceJoint2D joint = gameObject.AddComponent<DistanceJoint2D>();
            joint.connectedAnchor = point;
            joint.distance = 0;
            joint.enableCollision = false;
            joint.connectedBody = connectTo.GetComponent<Rigidbody2D>();
            joints.Add(joint);
        }
        
        private void AddSpringJoint(GameObject connectTo, Vector2 point)
        {
            SpringJoint2D joint = gameObject.AddComponent<SpringJoint2D>();
            joint.connectedAnchor = point;
            joint.distance = 0;
            joint.enableCollision = false;
            joint.frequency = 20;
            joint.dampingRatio = 20;
            joint.connectedBody = connectTo.GetComponent<Rigidbody2D>();
            joints.Add(joint);
        }
        
        public void ActivateConnections()
        {
            if(type == AnchorType.Regular)
            {
                GetComponent<Rigidbody2D>().isKinematic = false;
            }
            //Change the sprite rendering layer, so that the wagons show on top of all except the road anchors
            if(structureType == StructureType.Regular)
            {
                SpriteRenderer sr = GetComponent<SpriteRenderer>();
                sr.sortingLayerName = drawingManager.girderLayer;
                sr.sortingOrder = 1;
                
            }
        }
        
        public void DeactivateConnections()
        {
            GetComponent<Rigidbody2D>().isKinematic = true;
        }

        /// <summary>
        /// This function is used to destroy the anchor directly, if for some reason destroying the girder attached to it leaves it on the screen. (Should never happen, but does on a few random devices)
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerEnter(PointerEventData eventData)
        {
            if(type == AnchorType.Regular)
            {
                if(connections.Count == 0)
                {
                    DestroyAnchor();
                }
                bool shouldDestroy = true;
                foreach(GameObject go in connections)
                {
                    if(go != null)
                    {
                        shouldDestroy = false;
                    }
                }

                if(shouldDestroy)
                {
                    DestroyAnchor();
                }
            }
        }

        public void DestroyAnchor()
        {
            drawingManager.gameManager.levelData.anchors.RemoveAll(x => x.anchorName.Equals(gameObject.name));
            Destroy(gameObject);
        }
    }
    
    public enum AnchorType
    {
        Regular,
        BridgeStart,
        BridgeEnd,
        Static
    }
}