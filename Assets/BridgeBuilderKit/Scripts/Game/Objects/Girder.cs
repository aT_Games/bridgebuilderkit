﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System.Linq;

namespace BBK
{
    public class Girder : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler
    {

        public int materialUsagePerUnit = 4;
        public Transform point1Transform;
        public Transform point2Transform;
        
        public List<Anchor> connections = new List<Anchor>();
        
        public List<SpringJoint2D> joints = new List<SpringJoint2D>();
        public StructureType structureType;
        private SpriteRenderer sr;
        private bool valid = true;
        private float girderWidth;
        private float maxStrain = 200;
        private float currentStrain;
        private float newStrain;
        private DrawingManager drawingManager;

        public float GirderWidth
        {
            get
            {
                if(girderWidth == 0)
                {
                    GetGirderWidth();
                }
                return girderWidth;
            }

            set
            {
                girderWidth = value;
            }
        }

        // Use this for initialization
        void Awake()
        {
            drawingManager = FindObjectOfType<DrawingManager>();
            GetGirderWidth();
            sr = GetComponent<SpriteRenderer>();
        }

        // Update is called once per frame
        void Update()
        {
            if(drawingManager.gameManager.isPlaying)
            {
                //Check for the strain on the girder
                newStrain = 0;
                foreach(SpringJoint2D joint in joints)
                {
                    newStrain += joint.reactionForce.magnitude;
                }

                //Use lerp to get a smoother value
                currentStrain = Mathf.Lerp(currentStrain, newStrain, 4 * Time.deltaTime);

                if (newStrain > maxStrain)
                {
                    currentStrain = 0;
                    foreach(SpringJoint2D joint in joints)
                    {
                        joint.breakForce = 0;
                    }
                    joints = new List<SpringJoint2D>();
                }

                sr.color = new Color(1,1f - currentStrain / maxStrain, 1f - currentStrain / maxStrain);
            }
        }

        public float GetGirderWidth()
        {
            if(!valid)
            {
                return 0;
            }

            girderWidth = Vector2.Distance(point1Transform.position, point2Transform.position);
            
            //Round to the nearest 1/10th
            girderWidth *= 10;
            girderWidth = Mathf.Round(girderWidth);
            girderWidth /= 10;
            
            return girderWidth;
        }
        
        public int GetGirderMaterialUsage()
        {
            if(drawingManager == null)
            {
                drawingManager = FindObjectOfType<DrawingManager>();
            }
            
            float length = Vector2.Distance(point1Transform.position, point2Transform.position);
            float units = length/girderWidth;
            int roundedUnits = (int)units;
            float remainder = units - roundedUnits;
            int maxGirderUnits = (int)(drawingManager.maximumGirderLength/girderWidth);
            
            //Round up if needed
            if(remainder >= .5f)
            {
                roundedUnits++;
            }
            
            roundedUnits = Mathf.Clamp(roundedUnits, 1, maxGirderUnits);
            return roundedUnits * materialUsagePerUnit;
        }

        public void DrawBetween(Vector2 point1, Vector2 point2)
        {
            Vector2 centerPosition;
            Vector3 rotation;
            float scale;

            
                //Find girder position
                centerPosition = (point1 + point2) / 2f;
                gameObject.transform.position = centerPosition;

            if (Vector2.Distance(point1, point2) > .1f)
            {
                valid = true;
                //Find girder scale
                scale = Vector2.Distance(point1, point2) / GirderWidth;
                gameObject.transform.localScale = new Vector3(scale, 1, 1);

                //Find girder rotation
                rotation = (point2 - point1).normalized;
              
                //Check if we need to mirror the girder (drawing right to left instead of vice versa)
                if(point2.x < point1.x)
                {
                    gameObject.transform.right = -rotation;
                    gameObject.transform.Rotate(0,0,180);
                }
                else
                {
                      gameObject.transform.right = rotation;
                }
            }
            else
            {
                valid = false;
                gameObject.transform.position = new Vector2(1000,1000);
            }

        }
        public void SetStructureType(StructureType type)
        {
            structureType = type;
            if(structureType == StructureType.Regular)
            {
                gameObject.layer = LayerMask.NameToLayer(drawingManager.girderLayer);
                GetComponent<SpriteRenderer>().sprite = SpriteManager.GetDrawingSprite("girder");
            }
            else
            {
                gameObject.layer = LayerMask.NameToLayer("Road");
                GetComponent<SpriteRenderer>().sprite = SpriteManager.GetDrawingSprite("roadGirder");
            }
        }
        public void AddConnection(Anchor a)
        {
            Rigidbody2D body = GetComponent<Rigidbody2D>();
            connections.Add(a);
            joints.Add((SpringJoint2D)a.joints.FirstOrDefault(x => x.connectedBody == body));
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if(drawingManager.mode == DrawingMode.Deleting && Input.GetMouseButton(0))
            {
                drawingManager.RemoveGirder(this);
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if(drawingManager.mode == DrawingMode.Deleting)
            {
               drawingManager.RemoveGirder(this); 
            }
        }
        
        public void Deactivate()
        {
            GetComponent<Rigidbody2D>().isKinematic = true;
        }
    }
    
    public enum StructureType
    {
        Regular,
        Road
    }
}