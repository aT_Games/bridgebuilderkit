﻿using UnityEngine;
using System.Collections;

namespace  BBK
{
    [System.Serializable]
    public class GirderSaveData  {
        public string girderName;
        public Vector2 point1;
        public Vector2 point2;
        
        public GameObject gameObject;
        
        public GirderSaveData()
        {
            
        }
        
        public GirderSaveData(Girder g)
        {
            girderName = g.gameObject.name;
            point1 = g.point1Transform.position;
            point2 = g.point2Transform.position;
            gameObject = g.gameObject;
        }
    }
}
