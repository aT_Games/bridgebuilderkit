﻿using UnityEngine;
using System.Collections;

namespace BBK
{
    [System.Serializable]
    public class AnchorSaveData  {
        public string anchorName;
        public AnchorType type;
        public Vector2 position;
        public GameObject gameObject;
        
        public AnchorSaveData()
        {
            
        }
        
        public AnchorSaveData(Anchor a)
        {
            anchorName = a.gameObject.name;
            type = a.type;
            position = a.transform.position;
            gameObject = a.gameObject;
        }
    }
}
