﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BBK
{
    public class RunnerManager : MonoBehaviour
    {
        public List<Runner> runners;
        private int successfulRunners = 0;
        private int failedRunners = 0;
        private GameManager gameManager;
        void Start()
        {
            gameManager = FindObjectOfType<GameManager>();
            //Add delegates
            foreach(Runner r in runners)
            {
                r.Finished += OnRunnerFinish;
            }

            Invoke("FindRunnerStartingPoints", .2f);
        }
        
        // Use this for initialization
        public void StartRunners()
        {
            //Find the runners stopping points. This is dynamic based on the end of the bridge
            FindRunnersStopingPoints();
            
            foreach(Runner r in runners)
            {
                r.StartRun();
            }
        }
        
        public void StopRunners()
        {
            if(successfulRunners + failedRunners != runners.Count)
            {
                foreach(Runner r in runners)
                {
                    r.StopRun();
                }
                gameManager.RunnersFinished(successfulRunners);
            }
        }
        
        public void FindRunnerStartingPoints()
        {
            float trainLength = 0;
            //Go through this forwards, because the first unit in the line will have the shortest starting point.
            for(int i = 0; i < runners.Count; i++)
            {
                RaycastHit2D ground;
                Vector2 startPos = new Vector2(0, 10);
                
                //Starting point is the last bridge anchor, plus the width of the units after me, plus half of my width (because the pivot is in the center)
                startPos.x = gameManager.runnerStart.x - trainLength - (runners[i].unitWidth/2f);
                
                //Raycast downward to find the y value of the starting position;
                ground = Physics2D.Raycast(startPos, Vector2.down, Mathf.Infinity, LayerMask.GetMask("Road"));
                startPos.y = ground.point.y;
          
                runners[i].transform.position = startPos;
                trainLength += runners[i].unitWidth;
            }
        }
        
        private void FindRunnersStopingPoints()
        {
            float trainLength = .01f;
            //Go through this backwards, because the last unit in the line will have the shortest stopping point.
            for(int i = runners.Count - 1; i >= 0; i--)
            {
                //Stopping point is the last bridge anchor, plus the width of the units after me, plus half of my width (because the pivot is in the center)
                runners[i].stoppingPoint = gameManager.runnerEnd.x + trainLength + (runners[i].unitWidth/2f);
                trainLength += runners[i].unitWidth;
            }
        }
        
        void OnRunnerFinish(bool succcess)
        {
            if(succcess)
            {
                successfulRunners++;
            }
            else
            {
                failedRunners++;
            }
            
            if(successfulRunners + failedRunners == runners.Count)
            {
                foreach(Runner r in runners)
                {
                    r.StopRun();
                }
                gameManager.RunnersFinished(successfulRunners);
            }
        }
        
        void OnDestroy()
        {
            //Remove delegates
            foreach(Runner r in runners)
            {
                r.Finished -= OnRunnerFinish;
            }
        }
    }
}
