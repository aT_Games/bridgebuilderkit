﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;

namespace BBK
{
    public class DrawingManager : MonoBehaviour
    {
        [HideInInspector]
        public DrawingMode mode = DrawingMode.Building;
        public GameObject drawingPrefab;
        public bool snapDrawing;
        [TooltipAttribute("This value is in world units")]
        public int snapDegreeIncrements;
        public float maximumGirderLength;
        public Text lblAvailableGirders;
        public Camera inputCamera;
        public Transform anchorParent;
        public Transform girderParent;
        public GameObject anchorPrefab;
        public GameObject girderPrefab;
        public LayerMask girderValidationMask;
        public Color goodDrawColor;
        public Color badDrawColor;
        public string drawingLayer;
        public string girderLayer;
        public string anchorLayer;
        public GameManager gameManager;
        private bool isSnapping;
        private Girder drawingObject;
        private SpriteRenderer drawingSprite;
        private GameObject startDrawObject;
        private RaycastHit2D snapToObject;
        private Vector2 startDrawPosition;
        private Vector2 currentDrawPosition;
        private RaycastHit2D[] raycastHits;
        private int currentGirderUsage;
        void Awake()
        {
            if(gameManager == null)
            {
                gameManager = FindObjectOfType<GameManager>();
            }
        }

        public void UpdateUI()
        {
            lblAvailableGirders.text = (gameManager.levelData.boardCount - currentGirderUsage).ToString();
        }
        
        public void StartDraw(GameObject startObject)
        {
            startDrawObject = startObject;
            startDrawPosition = startObject.transform.position;
            drawingObject = GameObject.Instantiate(drawingPrefab).GetComponent<Girder>();
            drawingSprite = drawingObject.GetComponent<SpriteRenderer>();
            drawingObject.gameObject.layer = LayerMask.NameToLayer(drawingLayer);
            drawingObject.DrawBetween(startDrawPosition, startDrawPosition);
        }

        public void Drag(Vector2 dragTo, bool useTouchPosition = true)
        {
            if(useTouchPosition)
            {
                currentDrawPosition = inputCamera.ScreenToWorldPoint(Input.mousePosition);
            }
            else
            {
                currentDrawPosition = dragTo;
            }
            //Check for an anchor to snap to.
            raycastHits = Physics2D.RaycastAll(startDrawPosition, currentDrawPosition - startDrawPosition, Vector2.Distance(startDrawPosition, currentDrawPosition));

            snapToObject = raycastHits.FirstOrDefault(x => x.collider.gameObject != startDrawObject && x.collider.gameObject.GetComponent<Anchor>() != null);
            if (snapToObject.collider != null && (!useTouchPosition || Vector2.Distance(startDrawPosition, snapToObject.collider.transform.position) <= maximumGirderLength))
            {
                isSnapping = true;
                currentDrawPosition = snapToObject.collider.transform.position;
            }
            else
            {
                //Make sure to clear out the snapToObject, as it is used later
                snapToObject = new RaycastHit2D();
                isSnapping = false;
            }
            
            //If not snapping to an object, should we snap to the "grid"?
            if(snapDrawing && !isSnapping && useTouchPosition)
            {
                currentDrawPosition = SnapDrawingPosition(currentDrawPosition);
            }
            drawingObject.DrawBetween(startDrawPosition, currentDrawPosition);

            ValidateDrawing();
        }

        private Vector2 SnapDrawingPosition(Vector2 currentDrawPosition)
        {
            float length;
            Vector2 dir;
            float currentDegree;
            float remainder;
            int snappedDegree;
            
            //Snap angle
            currentDegree = Vector2.Angle(Vector2.up, currentDrawPosition - startDrawPosition);
            snappedDegree = ((int)(currentDegree/snapDegreeIncrements)) * snapDegreeIncrements;
            remainder = currentDegree - snappedDegree;
            
            //Snap up. Leave snapped degree alone to "snap" down.
            if(remainder > snapDegreeIncrements /2f)
            {
                snappedDegree += snapDegreeIncrements;
            }
            
            //Mirror rotation, since Vector2.Angle returns the smallest angle (always positive).
            if(currentDrawPosition.x > startDrawPosition.x)
            {
                snappedDegree *= -1;
            }
            
            Quaternion rot = Quaternion.AngleAxis(snappedDegree, Vector3.forward);
            dir = rot * Vector2.up;
            
            //Snap length
            length = (int)(Vector2.Distance(startDrawPosition, currentDrawPosition) / drawingObject.GirderWidth);
            length = Mathf.Clamp(length * drawingObject.GirderWidth, 0, maximumGirderLength);
            
            currentDrawPosition = startDrawPosition + (dir * length);
            
            return currentDrawPosition;
        }
        
        public Girder EndDraw(bool shouldSave = true, string girderName = null)
        {
            //Check to make sure the drawing is valid
            //If we aren't saving these girders, it means we are running at load time. Therefore have the CreateGirder function skip validation of length.
            if (!shouldSave || (ValidateDrawing() && drawingObject.GetGirderWidth() > 0))
            {
                drawingObject.gameObject.layer = girderParent.gameObject.layer;
                CreateGirderConnections();
                drawingObject.transform.SetParent(girderParent);
                Vector3 point = drawingObject.transform.localPosition;
                point.z = 0;
                drawingObject.transform.localPosition = point;
                drawingSprite.color = Color.white;
                if(girderName == null)
                {
                    drawingObject.gameObject.name = string.Format("Girder{0}", gameManager.levelData.girders.Count);
                }
                else
                {
                    drawingObject.gameObject.name = girderName;
                }
                if(shouldSave)
                {
                    gameManager.levelData.girders.Add(new GirderSaveData(drawingObject));
                }
                
                //Find girder material usage
                currentGirderUsage += drawingObject.GetGirderMaterialUsage();
                UpdateUI();
            }
            else
            {
                Destroy(drawingObject.gameObject);
            }

            //Clear the snapToObject, if we have not already done so
            snapToObject = new RaycastHit2D();

            return drawingObject;
        }
        
        private bool ValidateDrawing()
        {
            bool passed = true;

            //Check to make sure we have enough material
            if (drawingObject.GetGirderMaterialUsage() > (gameManager.levelData.boardCount - currentGirderUsage))
            {
                //We do not have enough girders
                passed = false;
            }
            else
            {
                //Check to see if we collide with anything else in the scene
                raycastHits = Physics2D.RaycastAll(startDrawPosition, currentDrawPosition - startDrawPosition, Vector2.Distance(startDrawPosition, currentDrawPosition), girderValidationMask);
                foreach (RaycastHit2D hit in raycastHits)
                {
                    Girder gTest = hit.collider.gameObject.GetComponent<Girder>();
                    if (gTest != null)
                    {
                        bool anchorCheck = false;

                        //Check if we are starting from the same anchor
                        if (gTest.connections.Contains(startDrawObject.GetComponent<Anchor>()))
                        {
                            anchorCheck = true;
                        }

                        //Check if we are ending on the same anchor
                        if (snapToObject.collider != null && gTest.connections.Contains(snapToObject.collider.GetComponent<Anchor>()))
                        {
                            //If the first and the second anchor is the same, this is invalid
                            if (anchorCheck)
                            {
                                passed = false;
                                break;
                            }
                            anchorCheck = true;
                        }

                        //If we share one and only one anchor, we are ok, otherwise not
                        if (!anchorCheck)
                        {
                            passed = false;
                            break;
                        }
                    }
                    else
                    {
                        //We are hitting the ground collider.
                        //Find if the point of collision is near a static anchor. If it is, allow it since it is probably the end of the girder.
                        //Else, don't allow drawing, since it will look like the girder is passing through the ground.
                        float dist;

                        dist = Vector2.Distance(hit.point, startDrawObject.transform.position);

                        if (dist > .3f)
                        {
                            passed = false;
                        }

                        if (passed && snapToObject.collider != null)
                        {
                            dist = Vector2.Distance(hit.point, snapToObject.collider.transform.position);

                            if (dist < .3f)
                            {
                                passed = false;
                            }
                        }
                    }
                }

                //Finally, check to make sure our end point isn't inside the ground
                //How we do this is check if the line between our start point and end point intersects with the ground an odd number of times. 
                if (raycastHits.Count(x => x.collider.GetComponent<Girder>() == null) % 2 != 0)
                {
                    passed = false;
                }
            }

            if (passed)
            {
                drawingSprite.color = goodDrawColor;
            }
            else
            {
                drawingSprite.color = badDrawColor;
            }
            
            return passed;
        }

        private void CreateGirderConnections()
        {
            //Connect the beginning of the girder first, we know it came from an anchor.
            startDrawObject.GetComponent<Anchor>().AddConnection(drawingObject.gameObject, drawingObject.point1Transform.localPosition);
            drawingObject.AddConnection(startDrawObject.GetComponent<Anchor>());
            //Check if the anchor we just connected to is part of the road
            CheckIfRoad(startDrawObject.GetComponent<Anchor>());

            //Then connect the end of the girder, which may be snapping to an anchor, or may be creating a new anchor.
            if (snapToObject.collider != null)
            {
                snapToObject.collider.gameObject.GetComponent<Anchor>().AddConnection(drawingObject.gameObject, drawingObject.point2Transform.localPosition);
                drawingObject.AddConnection(snapToObject.collider.gameObject.GetComponent<Anchor>());
                CheckIfRoad(snapToObject.collider.gameObject.GetComponent<Anchor>());
            }
            else
            {
                Anchor newAchor = CreateAnchor(drawingObject.point2Transform.position);
                
                CheckIfRoad(newAchor);
                //Connect to the newly created anchor
                newAchor.AddConnection(drawingObject.gameObject, drawingObject.point2Transform.localPosition);
                drawingObject.AddConnection(newAchor);
            }
        }
        
        public Anchor CreateAnchor(Vector2 point, bool shouldSave = true)
        {
            GameObject newAchor = GameObject.Instantiate(anchorPrefab);
            Anchor anchor = newAchor.GetComponent<Anchor>();
            newAchor.name = string.Format("Anchor {0},{1}", point.x, point.y);
            newAchor.transform.SetParent(anchorParent, true);
            newAchor.transform.localPosition = point;
            newAchor.GetComponent<Anchor>().type = AnchorType.Regular;

            if (shouldSave)
            {
                gameManager.levelData.anchors.Add(new AnchorSaveData(anchor));
            }
            
            return anchor;
        }
        
        public Anchor CreateAnchor(AnchorSaveData asd)
        {
            GameObject newAchor = GameObject.Instantiate(anchorPrefab);
            
            if(asd.type == AnchorType.Regular)
            {
                newAchor.GetComponent<SpriteRenderer>().sprite = SpriteManager.GetDrawingSprite("anchor");
            }
            else
            {
                newAchor.GetComponent<SpriteRenderer>().sprite = SpriteManager.GetDrawingSprite("staticAnchor");
            }
            
            Anchor script;
            newAchor.name = asd.anchorName = string.Format("Anchor {0},{1}", asd.position.x, asd.position.y);
            newAchor.transform.SetParent(anchorParent, true);
            newAchor.transform.localPosition = asd.position;
            script = newAchor.GetComponent<Anchor>();
            script.type = asd.type;
            
            //Make sure static anchors aren't interacting with the ground collider
            if(script.type != AnchorType.Regular)
            {
                Physics2D.IgnoreCollision(newAchor.GetComponent<Collider2D>(), gameManager.groundCollider, true);
            }
            
            return script;
        }
        
        //Check if the passed Anchor is part of the road or not
        public void CheckIfRoad(Anchor a)
        {
            if(a.type == AnchorType.BridgeStart || a.type == AnchorType.BridgeEnd)
            {
                a.structureType = StructureType.Road;
            }

            foreach(GameObject g in a.connections)
            {
                Girder girder = g.GetComponent<Girder>();

                if(girder != null && girder.structureType == StructureType.Road)
                {
                    a.structureType = StructureType.Road;
                }
            }
            
            //Something else could have already set this true;
            if(a.structureType == StructureType.Road)
            {
                //Check to see if we are flat (part of the road)
                foreach(GameObject g in a.connections)
                {
                    Girder girder = g.GetComponent<Girder>();
                    if(g.transform.rotation.eulerAngles.z < 20 || Mathf.Abs(g.transform.rotation.eulerAngles.z - 356) < 20 || Mathf.Abs(g.transform.rotation.eulerAngles.z - 180) < 20)
                    {
                        girder.SetStructureType(StructureType.Road);
                        
                        //Recursion, to make sure all the rest of the pieces of the road are setup
                        foreach(Anchor subAnchor in girder.connections)
                        {
                            if(subAnchor.structureType != StructureType.Road)
                            {
                                CheckIfRoad(subAnchor);
                            }
                        }
                    }
                }
            }
        }
        
        public void RemoveGirder(Girder girder)
        {
            //Remove from the list
            gameManager.levelData.girders.RemoveAll(x => x.girderName.Equals(girder.name));
            //Remove connections?
            foreach(Anchor anchor in girder.connections)
            {
                anchor.RemoveConnection(girder.gameObject);
                if(anchor.type == AnchorType.Regular && anchor.connections.Count == 0)
                {
                    anchor.DestroyAnchor();
                }
            }
            //Return used material
            currentGirderUsage -= girder.GetGirderMaterialUsage();
            UpdateUI();
            //Remove gameObject
            Destroy(girder.gameObject);
        }
    }
    
    public enum DrawingMode
    {
        Building,
        Deleting,
        Testing
    }
}