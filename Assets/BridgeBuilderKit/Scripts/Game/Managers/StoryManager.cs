﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace BBK
{
    [ExecuteInEditMode]
    public class StoryManager : MonoBehaviour
    {
        public static StoryManager instance;
        public GameObject storyPanel;
        public Image imgStoryImage;
        public UIButton btnNext;

        [Tooltip("Images appear in the order that they are placed in the list")]
        public List<Sprite> storyImages = new List<Sprite>();

        private int storyIndex = 0;

        void Start()
        {
            instance = this;

            btnNext.Click += AdvanceStory;
        }

        // Update is called once per frame
        void Update()
        {
#if UNITY_EDITOR

            if (instance == null)
            {
                instance = this;
            }

            //Check to make sure that the images we need in SpriteManager are placed there
            if (!Application.isPlaying && SpriteManager.instance != null)
            {
                UnityEditor.Undo.RecordObject(SpriteManager.instance, "Update Story Manager");
                foreach (Sprite story in storyImages)
                {
                    if (story != null)
                    {
                        if (!SpriteManager.instance.storySprites.Contains(story))
                        {
                            SpriteManager.instance.storySprites.Add(story);
                        }
                    }
                } 
            }
#endif
        }

        void ShowStory(bool shouldShow)
        {
            storyPanel.SetActive(shouldShow);
            if (shouldShow)
            {
                LoadStoryImage();
            }
        }

        void LoadStoryImage()
        {
            imgStoryImage.sprite = storyImages[storyIndex];

            //Change button text if it is the last slide
            if (storyIndex == storyImages.Count - 1)
            {
                btnNext.GetComponentInChildren<Text>().text = "Next";
            }
            else
            {
                btnNext.GetComponentInChildren<Text>().text = "Investigate";
            }

            //Change button text for the tutorial
            if (PlayerPrefs.GetString("CurrentLevelName", "Independence").Equals("Independence"))
            {
                btnNext.GetComponentInChildren<Text>().text = "Next";
            }
        }

        void AdvanceStory()
        {
            storyIndex++;
            if(storyIndex >= storyImages.Count)
            {
                ShowStory(false);
                return;
            }

            LoadStoryImage();
        }

        public List<string> GetStory()
        {
            List<string> listImgs = new List<string>();

            foreach(Sprite story in storyImages)
            {
                if(story != null)
                {
                    listImgs.Add(story.name);
                }
            }

            return listImgs;
        } 

        public void SetStory(List<string> listImgs)
        {
            Sprite newStory;

            storyImages = new List<Sprite>();

            foreach(string story in listImgs)
            {
                newStory = SpriteManager.GetStorySprite(story);

                if(newStory != null)
                {
                    storyImages.Add(newStory);
                }
            }
            
            //Make sure we only do this if we are playing the game
            if(Application.isPlaying && storyImages.Count > 0)
            {
                ShowStory(true);
            }
        }

        void OnDestroy()
        {
            btnNext.Click -= AdvanceStory;
        }
    }
}
