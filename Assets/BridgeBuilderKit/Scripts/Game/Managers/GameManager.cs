﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;
using System;

namespace BBK
{
    [ExecuteInEditMode]
    public class GameManager : MonoBehaviour
    {
        public SpriteRenderer backgroundRenderer;
        public EdgeCollider2D groundCollider;
        [TooltipAttribute("This is the Y value in world space, below which runners are considered fallen.")]
        public float fallLine = -1;
        public InputManager inputManager;
        public UIButton btnMainMenu;
        public UIButton btnBuild;
        public UIButton btnDelete;
        public UIButton btnPlay;
        public UIButton btnRestart;
        public EndLevelDialog endLevelDialog;
        public GameObject LevelTimerObject;
        public Text lblLevelTimer;
        public Transform coverParent;        
        public DrawingManager drawManager;
        public RunnerManager runnerManager;
        public string levelName = "Alcove Spring";
        public LevelData levelData;
        [HideInInspector]
        public Vector2 runnerEnd;
        [HideInInspector]
        public Vector2 runnerStart;
        [HideInInspector]
        public bool isPlaying;

        // Use this for initialization
        void Start()
        {

            levelName = PlayerPrefs.GetString("CurrentLevelName", "Independence");
            
            Invoke("DelayedStart", .2f);

#if UNITY_ANDROID || UNITY_IOS
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
#endif
        }

        //Delay the start when in play mode, to counteract the script execution order
        void DelayedStart()
        {
            if(drawManager == null)
            {
                drawManager = FindObjectOfType<DrawingManager>();
            }
            
            if(runnerManager == null)
            {
                runnerManager = FindObjectOfType<RunnerManager>();
            }
            
            AddButtonDelegates();
            
            //Make sure physics are off
            Physics2D.gravity = new Vector2(0, 0);
            
            LevelLoad();
        }

        // Update is called once per frame
        void Update()
        {
            //This includes the back button on Android
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                GoBack();
            }
        }
        
        public void LevelLoad()
        {
            LoadLevelData();
            LoadBackground();
            LoadCollider();
            LoadStaticAnchors();
            LoadCover();
            LoadSavedData();
            SetCameraBounds();
            drawManager.UpdateUI();
        }

        private void LoadCollider()
        {
            if(levelData.colliderPoints.Count > 1)
            {
                groundCollider.points = levelData.colliderPoints.ToArray();
            }
            else
            {
                //Handle legacy cases where there is no collider information yet
                Vector2[] arr = new Vector2[2];
                arr[0] = new Vector2(-6,0);
                arr[1] = new Vector2(6,0);
                groundCollider.points = arr;
            }
        }

        private void LoadSavedData()
        {
            int i = 0;
            //Load saved anchors first, otherwise the girder creation will try to make them.
            foreach(AnchorSaveData asd in levelData.anchors)
            {
               asd.gameObject = drawManager.CreateAnchor(asd.position, false).gameObject;
            }
            
            foreach(GirderSaveData gsd in levelData.girders)
            {
                //Find the starting anchor to draw from
                Collider2D[] col = Physics2D.OverlapCircleAll(gsd.point1, .1f, 1 << drawManager.anchorParent.gameObject.layer);
                //Reuse player draw methods to draw saved girders
                drawManager.StartDraw(col[0].gameObject);
                drawManager.Drag(gsd.point2, false);
                gsd.gameObject = drawManager.EndDraw(false, gsd.girderName).gameObject;
                i++;
            }
        }


        /// <summary>
        /// Load the level data
        /// </summary>
        void LoadLevelData()
        {
            if(Application.isPlaying)
            {
                //Only load saved girders if we are in play mode
                Debug.Log(string.Format("Loading: {0}/{1}{2}.txt", Application.persistentDataPath, LevelData.SAVEDATAPATH, levelName));
                levelData = new LevelData(Resources.Load<TextAsset>(string.Format("{0}{1}",LevelData.LEVELDATAPATH, levelName)),string.Format("{0}/{1}{2}.txt",Application.persistentDataPath,LevelData.SAVEDATAPATH, levelName));
            }
            else
            {
                levelData = new LevelData(Resources.Load<TextAsset>(string.Format("{0}{1}",LevelData.LEVELDATAPATH, levelName)));
            }
        }

        /// <summary>
        /// Load the background sprite using the level data
        /// </summary>
        void LoadBackground()
        {
            backgroundRenderer.gameObject.transform.localScale = new Vector2(levelData.scaleFactor, levelData.scaleFactor);
            backgroundRenderer.sprite = SpriteManager.GetBackgroundSprite(levelData.name);
            
            if(Application.isPlaying)
            {
                //If we are loading this level at runtime, make sure there is a collider
                backgroundRenderer.gameObject.AddComponent<BoxCollider2D>();
            }
            
            runnerStart = levelData.runnerStartPos;
            
            return;
        }

        void LoadStaticAnchors()
        {
            //Remove any anchors remaining from the previously loaded level
            List<Transform> children = drawManager.anchorParent.Cast<Transform>().ToList();
            foreach(Transform t in children)
            {
                DestroyImmediate(t.gameObject);
            }           
            
            foreach(AnchorSaveData anchor in levelData.startingAnchorPoints)
            {
                drawManager.CreateAnchor(anchor);
                
                //Check for the beginning of the bridge
                if(anchor.type == AnchorType.BridgeStart)
                {
                    runnerStart = anchor.position;
                    fallLine = runnerStart.y;
                }
                
                //Check for the end of the bridge
                if(anchor.type == AnchorType.BridgeEnd)
                {
                    runnerEnd = anchor.position;
                    if(runnerEnd.y < fallLine)
                    {
                        fallLine = runnerEnd.y;
                    }
                }

                //Calculate fallLine
                fallLine -= 1;
            }
        }

        void LoadCover()
        {
            string coverName;
            GameObject coverObject;
            SpriteRenderer sr;
            
            //Remove any cover objects remaining from the previously loaded level
            List<Transform> children = coverParent.Cast<Transform>().ToList();
            foreach(Transform t in children)
            {
                DestroyImmediate(t.gameObject);
            } 

            for(int i = 0; i < levelData.coverObjects.Count; i++)
            { 
                coverName = string.Format("cover_{0}_{1}", i + 1, levelData.name);
                coverObject = new GameObject(coverName);
                sr = coverObject.AddComponent<SpriteRenderer>();
                sr.sprite = SpriteManager.GetCoverSprite(coverName);
                sr.sortingLayerName = "Cover";
                coverObject.transform.SetParent(coverParent, false);

                //Fix issue, positions should be in world units
                Vector2 tempPos = levelData.coverObjects[i].position;
                if(levelData.versionNumber < 1)
                {
                    tempPos.x -= SpriteHelper.GetSpriteSize(backgroundRenderer.sprite).x / 2;
                }
                coverObject.transform.localPosition = levelData.coverObjects[i].position;
                coverObject.transform.localScale = new Vector2(levelData.scaleFactor, levelData.scaleFactor);
            }
        }
        
        public void SaveNewDefaultData()
        {
            bool hasError = false;
            
            //Update name, in case it is new
            levelData.name = levelName;
            PlayerPrefs.SetString("CurrentLevelName", levelName.Trim());
            
            //Update background sprite
            levelData.backgroundSprite = backgroundRenderer.sprite.name;
            levelData.scaleFactor = backgroundRenderer.transform.localScale.x;
            
            levelData.runnerStartPos = runnerStart;
            levelData.runnerEndPos =  runnerEnd;
            
            //Update anchors, in case they have changed
            levelData.startingAnchorPoints = new List<AnchorSaveData>();
            foreach(Transform t in drawManager.anchorParent)
            {
                Anchor a = t.GetComponent<Anchor>();
                
                //Only static/bridge anchors can be saved in default data
                if(a.type == AnchorType.Regular)
                {
                    a.type = AnchorType.Static;
                }
                
                levelData.startingAnchorPoints.Add(new AnchorSaveData(a));
            }
            
            //Update cover, in case it has changed
            levelData.coverObjects = new List<CoverSprite>();
            foreach(Transform t in coverParent)
            {
                levelData.coverObjects.Add(new CoverSprite(t.position.x, t.position.y, t.gameObject.GetComponent<SpriteRenderer>().sprite.name));
            }
            
            levelData.colliderPoints = groundCollider.points.ToList();
            
            //Validate starting anchors
            if(levelData.startingAnchorPoints.Where(x => x.type == AnchorType.BridgeStart).Count() != 1)
            {
                Debug.LogError("Level must have a single anchor of type BridgeStart");
                hasError = true;
            }
            if(levelData.startingAnchorPoints.Where(x => x.type == AnchorType.BridgeEnd).Count() != 1)
            {
                Debug.LogError("Level must have a single anchor of type BridgeEnd");
                hasError = true;
            }
            
            //Validate GroundCollider
            if(groundCollider.pointCount < 6)
            {
                Debug.LogError("GroundCollider must be configured");
                hasError = true;
            }
            
            if(hasError)
            {
                return;
            }
            else
            {
                //Clear error log
#if UNITY_EDITOR
                var logEntries = System.Type.GetType("UnityEditorInternal.LogEntries,UnityEditor.dll");
                var clearMethod = logEntries.GetMethod("Clear", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                clearMethod.Invoke(null,null);
#endif
            }
            
            levelData.SaveDefaultsToFile();
        }

        /// <summary>
        /// Set the camera bounds based off the size of the background sprite
        /// </summary>
        void SetCameraBounds()
        {
            Vector2 backgroundSize = SpriteHelper.GetSpriteSize(backgroundRenderer.sprite);
            float maxZoom = backgroundSize.y / 2f * levelData.scaleFactor; //Height in Unity units is 2 * size. Since we are working backwards to set the size of the camera, we do the opposite.

            inputManager.SetInputBounds(backgroundRenderer.bounds, maxZoom);
        }
        
        private IEnumerator LevelTimer()
        {
            int seconds = levelData.levelTimer;
            while(isPlaying && seconds > 0)
            {
                lblLevelTimer.text = seconds.ToString();
                yield return new WaitForSeconds(1 * Time.timeScale);
                seconds--;
            }
            
            //If we are still playing, that means we ran out of time
            if(isPlaying)
            {
                lblLevelTimer.text = "0";
                isPlaying = false;
                runnerManager.StopRunners();
            }
        }
        
        
        public void RunnersFinished(int score)
        {
            Debug.Log("Finished");
            isPlaying = false;
            Physics2D.gravity = Vector2.zero;
            //Stop the pieces from moving
            foreach(AnchorSaveData asd in levelData.anchors)
            {
                asd.gameObject.GetComponent<Anchor>().DeactivateConnections();
            }
            foreach(GirderSaveData gsd in levelData.girders)
            {
                gsd.gameObject.GetComponent<Girder>().Deactivate();
            }
            
            //Only update the score if it is higher
            if(PlayerPrefs.GetInt(string.Format("{0}_score", levelData.name), 0) < score)
            {
                PlayerPrefs.SetInt(string.Format("{0}_score", levelData.name), score);
            }
            PlayerPrefs.SetInt(string.Format("{0}_completed", levelData.name), 1);
            
            
            endLevelDialog.SetScore(score);
            endLevelDialog.ShowDialog(true);
        }

        void SwitchToBuildMode()
        {
            //Rename girders in list, so that the names don't get duplicated and continue to make sense
            for(int i = 0; i < levelData.girders.Count; i++)
            {
                levelData.girders[i].gameObject.name = levelData.girders[i].girderName = string.Format("Girder{0}", i);
            }
            drawManager.mode = DrawingMode.Building;
            btnBuild.transform.Find("Halo").gameObject.SetActive(true);
            btnDelete.transform.Find("Halo").gameObject.SetActive(false);
        }
        
        void SwitchToDeleteMode()
        {
            drawManager.mode = DrawingMode.Deleting;
            btnBuild.transform.Find("Halo").gameObject.SetActive(false);
            btnDelete.transform.Find("Halo").gameObject.SetActive(true);
        }
        
        void StartPlayMode()
        {
            isPlaying = true;
            
            //Swap UI
            btnMainMenu.gameObject.SetActive(false);
            btnBuild.gameObject.SetActive(false);
            btnDelete.gameObject.SetActive(false);
            btnPlay.gameObject.SetActive(false);
            btnRestart.gameObject.SetActive(true);
            LevelTimerObject.SetActive(true);
            lblLevelTimer.text = levelData.levelTimer.ToString();
            
            //Save level data
            levelData.SaveLevelDataToFile();
            
            //Activate joints
            foreach(AnchorSaveData asd in levelData.anchors)
            {
                asd.gameObject.GetComponent<Anchor>().ActivateConnections();
            }
            
            //Turn on physics
            Physics2D.gravity = new Vector2(0, -.5f);
            
            //Start runners
            runnerManager.StartRunners();
            
            //Start timer
            StartCoroutine(LevelTimer());

            //Set camera to following runners, in this case, the middle one.
            Transform runner = runnerManager.runners[(int)runnerManager.runners.Count / 2].transform;
            inputManager.CameraFollow(runner);
        }

        public void Skip()
        {
            PlayerPrefs.SetInt(string.Format("{0}_skipped", levelData.name), 1);
            MainMenu();
        }

        public void Restart()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        
        public void GoBack()
        {
            levelData.SaveLevelDataToFile();
            MainMenu();
        }

        public void MainMenu()
        {
            SceneManager.LoadScene("Launcher");
        }
        
        private void AddButtonDelegates()
        {
            //Setup buttons
            btnMainMenu.Click += GoBack;
            btnBuild.Click += SwitchToBuildMode;
            btnDelete.Click += SwitchToDeleteMode;
            btnPlay.Click += StartPlayMode;
            btnRestart.Click += Restart;
        }
        
        private void RemoveButtonDelegates()
        {
            //Setup buttons
            btnMainMenu.Click -= GoBack;
            btnBuild.Click -= SwitchToBuildMode;
            btnDelete.Click -= SwitchToDeleteMode;
            btnPlay.Click -= StartPlayMode;
            btnRestart.Click -= Restart;
        }
        
        void OnDestroy()
        {
            RemoveButtonDelegates();
        }
    }

    public class SpriteHelper
    {
        public static Vector2 GetSpriteSize(Sprite sprite)
        {
            Bounds bounds = sprite.bounds;
            return bounds.size;
        }
    }
    
}
