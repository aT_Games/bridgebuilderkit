﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


/// <summary>
/// This class is mainly for doing gernal UI things like loading button icons. 
/// Functionality of the individual UI components is handled by their own classes
/// </summary>
namespace BBK
{
    [ExecuteInEditMode]
    public class SpriteManager : MonoBehaviour
    {
        public static SpriteManager instance;

        public List<Sprite> backgroundSprites = new List<Sprite>();
        public List<Sprite> coverSprites = new List<Sprite>();
        public List<Sprite> drawingSprites = new List<Sprite>();
        public List<Sprite> storySprites = new List<Sprite>();

        void Start()
        {
            instance = this;
        }
        
        #if UNITY_EDITOR
        void Update()
        {
            if(instance == null)
            {
                instance = this;
            }
        }
        #endif

        /// <summary>
        /// Return a sprite by the given name. This is the general method, for speed use one of the specific methods
        /// </summary>
        /// <param name="spriteName"></param>
        /// <returns></returns>
        public static Sprite GetSprite(string spriteName)
        {
            if (SpriteManager.instance == null)
            {
                Debug.LogError("UIManager not found");
                return null;
            }

            //This method will have to search each defined list, if you impliment multiple lists
            Sprite newSprite = GetBackgroundSprite(spriteName);
            if (newSprite == null)
            {
                newSprite = GetCoverSprite(spriteName);
            }
            if (newSprite == null)
            {
                newSprite = GetDrawingSprite(spriteName);
            }
            if (newSprite == null)
            {
                newSprite = GetStorySprite(spriteName);
            }
            return newSprite;
        }


        /// <summary>
        /// Returns a background sprite by the given name
        /// </summary>
        /// <param name="spriteName"></param>
        /// <returns></returns>
        public static Sprite GetBackgroundSprite(string spriteName)
        {
            spriteName = spriteName.ToLower();
            spriteName = spriteName.Trim();
            if (SpriteManager.instance == null)
            {
                Debug.LogError("UIManager not found");
                return null;
            }
            Sprite newSprite = SpriteManager.instance.backgroundSprites.FirstOrDefault(x => x.name.ToLower().Equals(spriteName));
            if (newSprite == null)
            {
                Debug.LogError(string.Format("Sprite {0} not found in {1} backgroundSprites", spriteName, SpriteManager.instance.gameObject.name));
            }
            return newSprite;
        }

        /// <summary>
        /// Returns a cover sprite by the given name
        /// </summary>
        /// <param name="spriteName"></param>
        /// <returns></returns>
        public static Sprite GetCoverSprite(string spriteName)
        {
            spriteName = spriteName.ToLower();
            if (SpriteManager.instance == null)
            {
                Debug.LogError("UIManager not found");
                return null;
            }
            Sprite newSprite = SpriteManager.instance.coverSprites.FirstOrDefault(x => x.name.ToLower().Equals(spriteName));
            if (newSprite == null)
            {
                Debug.LogError(string.Format("Sprite {0} not found in {1} coverSprites", spriteName, SpriteManager.instance.gameObject.name));
            }
            return newSprite;
        }
        
        /// <summary>
        /// Returns a drawing sprite by the given name
        /// </summary>
        /// <param name="spriteName"></param>
        /// <returns></returns>
        public static Sprite GetDrawingSprite(string spriteName)
        {
            spriteName = spriteName.ToLower();
            if (SpriteManager.instance == null)
            {
                Debug.LogError("UIManager not found");
                return null;
            }
            Sprite newSprite = SpriteManager.instance.drawingSprites.FirstOrDefault(x => x.name.ToLower().Equals(spriteName));
            if (newSprite == null)
            {
                Debug.LogError(string.Format("Sprite {0} not found in {1} drawingSprites", spriteName, SpriteManager.instance.gameObject.name));
            }
            return newSprite;
        }

        /// <summary>
        /// Returns a story sprite by the given name
        /// </summary>
        /// <param name="spriteName"></param>
        /// <returns></returns>
        public static Sprite GetStorySprite(string spriteName)
        {
            spriteName = spriteName.ToLower();
            if (SpriteManager.instance == null)
            {
                Debug.LogError("UIManager not found");
                return null;
            }
            Sprite newSprite = SpriteManager.instance.storySprites.FirstOrDefault(x => x.name.ToLower().Equals(spriteName));
            if (newSprite == null)
            {
                Debug.LogError(string.Format("Sprite {0} not found in {1} storySprites", spriteName, SpriteManager.instance.gameObject.name));
            }
            return newSprite;
        }
    }
}