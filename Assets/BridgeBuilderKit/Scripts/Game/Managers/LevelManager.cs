﻿using UnityEngine;
using UnityEngine.UI;
#if UNITY_ANDROID || UNITY_IOS
using UnityEngine.Advertisements;
#endif
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace BBK
{
    public class LevelManager : MonoBehaviour
    {
        public static LevelManager instance;

        public TextAsset levelData;
        
        //Buttons
        public UIButton btnSwitchLevelNext;
        public UIButton btnSwitchLevelLast;
        public UIButton btnLaunch;

        //Level information panel
        public Image levelImage;
        public Text lblLevelName;
        public Transform scorePanel;
        public Color scoreColorAchieved;
        public Color scoreColorFailed;

        //Level indicators
        public Transform levelIndicators;
        public Sprite lockedLevelSprite;
        public Sprite activeLevelSprite;
        public Sprite completedLevelSprite;

        private List<LevelData> levels = new List<LevelData>();

        private LevelData currentlySelectedLevel;
        // Use this for initialization
        void Start()
        {
            instance = this;

            //Add button delegates
            AddButtonDelegates();
            
            if(levelData == null)
            {
                Debug.LogError(string.Format("{0} is missing levelData!", gameObject.name));
            }
            if(levelIndicators == null || levelIndicators.childCount < 1)
            {
                Debug.LogError(string.Format("{0} is missing level indicators!", gameObject.name));
            }

            LoadData();

            //If the currently shown level is not the first or second level of the game, and it is an odd numbered level, show an ad
            //Remember that the list index is 0 based, so we have to add 1 to get the level number
            if(levels.IndexOf(currentlySelectedLevel) > 1 && (levels.IndexOf(currentlySelectedLevel) + 1) % 2 == 0)
            {
                ShowAd();
            }

        }

        //Method to show ads
        void ShowAd()
        {
#if UNITY_ANDROID || UNITY_IOS
            if(Advertisement.IsReady())
            {
                Advertisement.Show();
            }
#endif
        }

        /// <summary>
        /// This method loads the levels by name from the supplied list. It also gets whether the level was completed or skipped via PlayerPrefs, and associated the level indicator with its level.
        /// </summary>
        void LoadData()
        {
            string[] levelNames = levelData.text.Split('\n');

            if(levelIndicators.childCount < levelNames.Length)
            {
                Debug.LogError(string.Format("{0} levels in {1}, only {2} indicators exist in {3}", levelNames.Length, levelData.name, levelIndicators.childCount, levelIndicators.name));
            }

            for (int i = 0; i < levelNames.Length; i++)
            {
                LevelData ld = new LevelData();
                ld.name = levelNames[i].Trim();
                ld.levelIndicator = levelIndicators.GetChild(i).gameObject;
                ld.isCompleted = PlayerPrefs.GetInt(string.Format("{0}_completed", ld.name), 0) == 1;
                ld.isSkipped = PlayerPrefs.GetInt(string.Format("{0}_skipped", ld.name), 0) == 1;

                //In this example, the currently loading level is unlocked by having completed or skipped the previous level
                if (i == 0)
                {
                    ld.isUnlocked = true;
                    LoadLevelIndicator(ld, !ld.isCompleted && !ld.isSkipped);
                }
                else
                {
                    ld.isUnlocked = levels[i - 1].isCompleted || levels[i - 1].isSkipped;
                    LoadLevelIndicator(ld, ld.isUnlocked && !ld.isCompleted && !ld.isSkipped);
                }

                levels.Add(ld);
            }
        }

        /// <summary>
        /// This method loads the appropriate level indicator sprite for the level
        /// </summary>
        /// <param name="ld"></param>
        void LoadLevelIndicator(LevelData ld, bool setActive)
        {
            SpriteRenderer sr = ld.levelIndicator.GetComponent<SpriteRenderer>();

            //Make sure the level indicator has a sprite renderer
            if(sr == null)
            {
                sr = ld.levelIndicator.AddComponent<SpriteRenderer>();
            }

            //TODO, show a different sprite if the level was skipped instead of completed
            if(setActive)
            {
                sr.sprite = activeLevelSprite;
                sr.color = Color.white;
            }
            else if (ld.isCompleted || ld.isSkipped)
            {
                sr.sprite = completedLevelSprite;
            }
            else if(!ld.isUnlocked)
            {
                sr.sprite = lockedLevelSprite;
            }
            else
            {
                sr.sprite = completedLevelSprite;
            }
            
            if(setActive)
            {
                currentlySelectedLevel = ld;
                LoadLevelInformation();
            }

        }

        /// <summary>
        /// Load the visual level information
        /// </summary>
        void LoadLevelInformation()
        {
            Image sr;
            
            int levelScore = PlayerPrefs.GetInt(string.Format("{0}_score", currentlySelectedLevel.name), 0);
            int i = 0;
            levelImage.sprite = SpriteManager.GetBackgroundSprite(currentlySelectedLevel.name);
            lblLevelName.text = currentlySelectedLevel.name;

            foreach(Transform t in scorePanel)
            {
                i++;
                sr = t.gameObject.GetComponent<Image>();
                if(sr != null)
                {
                    if(i <= levelScore)
                    {
                        sr.color = scoreColorAchieved;
                    }
                    else
                    {
                        sr.color = scoreColorFailed;
                    }
                }
            }
        }

        void SwitchLevelNext()
        {
            SwitchSelectedLevel(1);
        }
        
        void SwitchLevelLast()
        {
            SwitchSelectedLevel(-1);
        }

        /// <summary>
        /// Increase or decrease the currently selected level
        /// </summary>
        /// <param name="direction">+1 to increase, -1 to decrease</param>
        void SwitchSelectedLevel(int direction)
        {
            int currentSelectedIndex = levels.IndexOf(currentlySelectedLevel);

            //Advance the selection
            currentSelectedIndex += direction;

            //Make sure we don't go out of range
            currentSelectedIndex = Mathf.Clamp(currentSelectedIndex, 0, levels.Count - 1);

            //Make sure the next level can be selected
            if (levels[currentSelectedIndex].isUnlocked)
            {
                LoadLevelIndicator(currentlySelectedLevel, false);
                currentlySelectedLevel = levels[currentSelectedIndex];
                LoadLevelIndicator(currentlySelectedLevel, true);
            }
        }

        /// <summary>
        /// Launches the currently selected level
        /// </summary>
        void LaunchLevel()
        {
            PlayerPrefs.SetString("CurrentLevelName", currentlySelectedLevel.name);
            SceneManager.LoadScene("Bridge");
        }
        
        void AddButtonDelegates()
        {
            btnSwitchLevelNext.Click += SwitchLevelNext;
            btnSwitchLevelLast.Click += SwitchLevelLast;
            btnLaunch.Click += LaunchLevel;
        }
        
        void RemoveButtonDelegates()
        {
            btnSwitchLevelNext.Click -= SwitchLevelNext;
            btnSwitchLevelLast.Click -= SwitchLevelLast;
            btnLaunch.Click -= LaunchLevel;
        }
        
        void OnDestroy()
        {
            RemoveButtonDelegates();
        }
    }
}
