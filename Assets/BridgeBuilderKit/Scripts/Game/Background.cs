﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

/// <summary>
/// This class is used to pass through information about drags on the background
/// </summary>
namespace BBK
{
    public class Background : MonoBehaviour, IDragHandler
    {
        private InputManager inputManager;
        private DrawingManager drawingManager;
        void Awake()
        {
            inputManager = FindObjectOfType<InputManager>();
            drawingManager = FindObjectOfType<DrawingManager>();
        }

        public void OnDrag(PointerEventData eventData)
        {
            if(drawingManager.mode == DrawingMode.Building)
            {
                inputManager.OnDrag(eventData);
            }
        }
    }
}
