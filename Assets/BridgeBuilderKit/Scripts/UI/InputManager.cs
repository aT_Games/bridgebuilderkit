﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
namespace BBK
{
    public class InputManager : MonoBehaviour
    {

        public static InputManager instance;

        [Tooltip("What percentage of the maximum zoom would you like the minium to be? Ex: .5f means you can zoom in twice as far.")]
        public float zoomPercentage = .5f;
        public float zoomSpeed = 6;
        public float maxZoom;
        public float minZoom;
        public float dragSpeed = 2f;
        public float acceleration = 4;
        public Camera inputCamera;



        //Zoom to cursor variables
        private float previousZoom;
        private Vector2 prevCursorPos;
        private Vector2 cursorDelta;
        private float scrollTarget;
        private float scrollDelta;
        private Vector3 zoomCenter;

        //Drag unit variables
        private Vector2 p1;
        private Vector2 p2;
        float worldUnit;


        private Vector2 startDragPosition;
        private Vector3 dragDiff;
        private Vector2 targetCameraPosition;
        private Bounds inputBounds;
        private float[] relativeBounds;
        private bool hasBounds;
        private bool isDragging = false;
        //This can be switched off by certain full screen panels. Expecially useful for ones that use their own dragging or scrolling to display content
        internal bool acceptInput = true;

        // Use this for initialization
        void Start()
        {
            instance = this;
            if (inputCamera == null)
            {
                inputCamera = Camera.main;
            }

            scrollTarget = inputCamera.orthographicSize;
            targetCameraPosition = inputCamera.transform.position;

#if UNITY_EDITOR
            if (inputCamera.aspect != (16f/9f))
            {
                Debug.LogWarning(string.Format("Detected aspect: {0} Currently only 16:9 is supported", inputCamera.aspect));
            }
#endif
        }

        // Update is called once per frame
        void Update()
        {
            UpdateZoom();
            UpdateDrag();
        }


        void UpdateZoom()
        {
            //TODO add perspective camera
#if UNITY_STANDALONE || UNITY_EDITOR
            if (acceptInput)
            {
                scrollDelta = Input.GetAxis("Mouse ScrollWheel");
            }
            else
            {
                scrollDelta = 0;
            }
            scrollTarget -= scrollDelta * zoomSpeed;
            scrollTarget = Mathf.Clamp(scrollTarget, minZoom, maxZoom);
            Vector3 zoomCenter = Input.mousePosition;

#endif
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
        // If there are two touches on the device...
        if (acceptInput && Input.touchCount == 2)
        {
            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            scrollDelta = prevTouchDeltaMag - touchDeltaMag;
			scrollTarget += scrollDelta * zoomSpeed * Time.deltaTime;
			scrollTarget = Mathf.Clamp(scrollTarget, minZoom, maxZoom);

            //Find focal point of the zoom
            zoomCenter = (touchZero.position + touchOne.position)/2;
        }
        else
        {
            scrollDelta = 0;
        }
#endif

            prevCursorPos = inputCamera.ScreenToWorldPoint(zoomCenter);
            inputCamera.orthographicSize = Mathf.Lerp(inputCamera.orthographicSize, scrollTarget, acceleration * Time.deltaTime);

            // Only do this if we are scrolling
            if (Mathf.Abs(scrollTarget - inputCamera.orthographicSize) > 0.001f)
            {

                // Recalculate the camera bounds, if there are any
                if (hasBounds)
                {
                    SetRelativeBounds();
                }

                //Make sure that if we start dragging before the zoom lerp has completed, that we don't fight with the drag position.
                if (!isDragging)
                {
                    // Update size and find the world delta
                    cursorDelta = prevCursorPos - (Vector2)inputCamera.ScreenToWorldPoint(zoomCenter);

                    // Update camera position to keep cursor/fingers centered, using the drag target. 
                    targetCameraPosition += cursorDelta;
                }

                //Clamp the drag target values before assigning them to the camera.
                ClampCameraPosition();

                if (!isDragging)
                {
                    inputCamera.transform.position = targetCameraPosition;
                }
            }
        }

        void UpdateDrag()
        {
            // Clamp first so that targetDragPosition guaranteed to be in bounds
            ClampCameraPosition();
            inputCamera.transform.position = Vector2.Lerp(inputCamera.transform.position, targetCameraPosition, acceleration * Time.deltaTime);

            if ((Vector2)inputCamera.transform.position == targetCameraPosition)
            {
                isDragging = false;
            }
        }

        //Makes sure the camera can't be moved outside of the bounds.
        void ClampCameraPosition()
        {
            if (hasBounds)
            {
                //Make sure our drag position is inside the current bounds.
                targetCameraPosition.x = Mathf.Clamp(targetCameraPosition.x, relativeBounds[0], relativeBounds[1]);
                targetCameraPosition.y = Mathf.Clamp(targetCameraPosition.y, relativeBounds[2], relativeBounds[3]);
            }
        }

        //This method processes drag via "click and drag" and touch.
        public void OnDrag(PointerEventData data)
        {
#if UNITY_ANDROID || UNITY_IOS
            if (!acceptInput || Input.touchCount > 1)
            {
                return;
            }
#endif

            //Find the current screen to world unit ratio
            p1 = inputCamera.ScreenToWorldPoint(Vector2.zero);
            p2 = inputCamera.ScreenToWorldPoint(Vector2.right);
            worldUnit = Vector2.Distance(p1, p2);

            //Do the conversion
            data.delta *= worldUnit;

            isDragging = true;

            //Move the camera
            targetCameraPosition = targetCameraPosition - data.delta;
        }

        /// <summary>
        /// Set the camera to following an object
        /// </summary>
        /// <param name="target"></param>
        public void CameraFollow(Transform target)
        {
            StartCoroutine(FollowTarget(target));
        }

        /// <summary>
        /// Move the camera into the starting position
        /// </summary>
        void StartCamera()
        {
            //This is as easy as setting the x value to a ridiculous number, and calling the clamp function.
            targetCameraPosition = inputCamera.transform.position;
            targetCameraPosition.x = -100;
            ClampCameraPosition();
        }


        /// <summary>
        /// Camera bounds depends on the size or field of view. Since players have the ability to zoom, the camera's bounds change.
        /// This method finds the bounds relative to the current size of the camera
        /// </summary>
        void SetRelativeBounds()
        {
            float vertExtent;
            float horzExtent;

            vertExtent = scrollTarget;
            horzExtent = vertExtent * Screen.width / Screen.height;

            relativeBounds = new float[4];
            relativeBounds[0] = horzExtent - inputBounds.size.x / 2f; //Left
            relativeBounds[1] = inputBounds.size.x / 2f - horzExtent; //Right
            relativeBounds[2] = vertExtent - inputBounds.size.y / 2f; //Bottom
            relativeBounds[3] = inputBounds.size.y / 2f - vertExtent; //Top
        }

        /// <summary>
        /// Define the bounds and zoom for the game camera. Without this, the camera will be able to go "off-screen"
        /// </summary>
        /// <param name="myInputBounds"></param>
        /// <param name="myMaxZoom"></param>
        public void SetInputBounds(Bounds myInputBounds, float myMaxZoom)
        {
            hasBounds = true;
            maxZoom = scrollTarget = inputCamera.orthographicSize = myMaxZoom;
            minZoom = maxZoom * zoomPercentage;
            inputBounds = myInputBounds;

            SetRelativeBounds();
            StartCamera();
        }

        IEnumerator FollowTarget(Transform target)
        {
            while(true)
            {
                targetCameraPosition = target.position;
                ClampCameraPosition();
                yield return null;
            }
        }
    }
}