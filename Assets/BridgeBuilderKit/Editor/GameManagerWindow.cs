﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections;

namespace BBK
{
    [CustomEditor(typeof(GameManager))]
    public class GameManagerWindow : Editor {
        
        public override void OnInspectorGUI()
        {
            GameManager gm;
            GameManager[] gms = FindObjectsOfType(typeof(GameManager)) as GameManager[];
            
                        
            if(gms.Length != 1)
            {
                Debug.LogError("Please make sure there is at least (and only) one GameManager in the scene.");
                return;
            }
            else
            {
                gm = gms[0];
            }
                        
            DrawDefaultInspector();
            Rect loadButtonRect = EditorGUILayout.BeginHorizontal("Button");
                if(GUI.Button(loadButtonRect, GUIContent.none))
                {
                    
                    string path = EditorUtility.OpenFilePanelWithFilters("Load Level Data", string.Format("{0}/{1}{2}",Application.dataPath, LevelData.RESOURCEPATH, LevelData.LEVELDATAPATH), new string[]{"Text","txt"});
                    if(path != null && path.Length > 0)
                    {
                        string[] pathData = path.Split('/');
                        string levelName = pathData[pathData.Length -1].Split('.')[0];
                        PlayerPrefs.SetString("CurrentLevelName", levelName);
                        gm.levelName = levelName;
                        gm.LevelLoad();
                    }
                }
            GUILayout.Label("Load Level Data");
            
            EditorGUILayout.EndHorizontal();
            
            Rect saveButtonRect = EditorGUILayout.BeginHorizontal("Button");
            if(GUI.Button(saveButtonRect, GUIContent.none))
            {
                
                string path = string.Format("{0}/{1}{2}{3}.txt",Application.dataPath, LevelData.RESOURCEPATH, LevelData.LEVELDATAPATH, gm.levelName);
                
                if(!File.Exists(path) || EditorUtility.DisplayDialog("Level Already Exists", string.Format("Are you sure you want to overwrite the level data for {0}?", gm.levelName), "Yes", "No"))
                {
                    gm.SaveNewDefaultData();
                    AssetDatabase.Refresh();
                }
            }
            GUILayout.Label("Save Level Data");
            EditorGUILayout.EndHorizontal();
        }
    }
}
